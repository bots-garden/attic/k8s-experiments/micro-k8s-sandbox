
REGISTRY_NAME = "sandbox-docker-registry"
REGISTRY_IP = "172.16.245.160"
REGISTRY_PUBLIC_IP = "192.168.1.160"
REGISTRY_DOMAIN = "registry.test"
REGISTRY = "registry.test:5000"

K8S_SERVER_NAME="sandbox-k8s-server"
K8S_SERVER_DOMAIN = "k8s.test"
K8S_SERVER_IP="172.16.245.138"
K8S_SERVER_PUB_IP = "192.168.1.138"

GITLAB_NAME = "sandbox-gitlab"
GITLAB_DOMAIN = "gitlab.test"
GITLAB_IP = "172.16.245.122"
GITLAB_PUB_IP = "192.168.1.122"

# = CI TOKENS
# = Admin (for Shared Runner) token
# go to http://gitlab.test/admin/runners
CI_REGISTRATION_TOKEN="B2xxgmiG_s22kzCyCaxz"
CI_REGISTRATION_URL="http://gitlab.test/"

RUNNERS_NAME="sandbox-runners"
RUNNERS_IP="172.16.245.211"
RUNNERS_PUB_IP = "192.168.1.211"
SHELL_RUNNER_TAG = "shell"
DOCKER_RUNNER_TAG = "docker"

